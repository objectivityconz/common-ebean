package nz.co.objectivity.common.ebean

import com.avaje.ebeaninternal.api.ClassPathSearchService
import com.avaje.ebeaninternal.server.util.ClassPathReader
import com.avaje.ebeaninternal.server.util.ClassPathSearchFilter
import com.avaje.ebeaninternal.server.util.ClassPathSearchMatcher
import com.avaje.ebeaninternal.server.util.DefaultClassPathReader
import groovy.transform.CompileStatic
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.loader.jar.JarEntryData
import org.springframework.boot.loader.jar.JarFile

import java.nio.charset.Charset

/**
 * An enhanced ClassPathReader for bathe-war
 */
@CompileStatic
public class BatheClassPathReader implements ClassPathSearchService {

  private static final Logger log = LoggerFactory.getLogger(BatheClassPathReader)

  private ClassLoader classLoader

  private ClassPathSearchFilter filter

  private ClassPathSearchMatcher matcher

  private HashSet<String> jarHits = new HashSet<String>();

  private HashSet<String> packageHits = new HashSet<String>();

  private ClassPathReader classPathReader = new DefaultClassPathReader();

  private List<Object> classPath = new ArrayList<Object>();

  private ArrayList<Class<?>> matchList = new ArrayList<Class<?>>();

  private List<String> interestingJars;

  private List<String> interestingPackages;

  public BatheClassPathReader() {
	SystemPropertyWrapper propertyWrapper = new SystemPropertyWrapper()

	String jarProperty = propertyWrapper.get("search.jars", propertyWrapper.get("jars", null))
	interestingJars = getSearchJarsPackages(jarProperty)

	String packageProperty = propertyWrapper.get("packages", null)
	interestingPackages = getSearchJarsPackages(packageProperty)
  }

  @Override
  void init(ClassLoader classLoader, ClassPathSearchFilter filter, ClassPathSearchMatcher matcher, String classPathReaderClassName) {
	this.classLoader = classLoader;
	this.filter = filter;
	this.matcher = matcher;
	initClassPaths();
  }

  public static List<String> getSearchJarsPackages(String searchPackages) {
	return searchPackages ? searchPackages.split("[ ,;]").collect({ String p ->
	  return p.trim().toLowerCase()
	}) : Collections.emptyList()
  }

  private Object[] readPath(ClassLoader classLoader) {
	List<URL> allResources = new ArrayList<>()

	List<URL> urlList = getURLsFromClassLoader(classLoader)

	if (urlList?.size() > 0) {
	  urlList.each({ URL url ->
		if (checkInterest(url)) {
		  allResources.add(url)
		}
	  })
	}

	return allResources.toArray()
  }

  /**
   * Export an embedded JAR file to the tmpFile, and add the absolute path as interesting resource path
   *
   * @param url is JarFile URL
   * @return a collection of interest absolute path
   */
  protected List<URL> exportEmbeddedJarByURL(URL url) {
	List<URL> interestingResource = new ArrayList<URL>();


	return interestingResource
  }

  /**
   * Get all resources under current ClassLoader
   *
   * @param classLoader is current ClassLoader
   * @return a collection of resources
   */
  protected List<URL> getURLsFromClassLoader(ClassLoader classLoader) {
	URLClassLoader ucl = (URLClassLoader) classLoader
	return Arrays.asList(ucl.getURLs())
  }

  /**
   * Check current url is interested to Ebean or not
   * @param url will be checked
   * @return true if it is interested to Ebean
   */
  protected boolean checkInterest(URL url) {
	return checkInterest(url.path)
  }

  /**
   * Check current path is interested to Ebean or not
   * @param path will be checked
   * @return true if it is interested to Ebean
   */
  protected boolean checkInterest(String path) {
	return interestingJars.any({ String jar -> return path?.toLowerCase().contains(jar) }) ||
			interestingPackages.any({ String pack -> return path?.toLowerCase().contains(pack) }) ||
			path.endsWith("/target/test-classes") ||
			path.endsWith("/target/classes")
  }

  @Override
  List<Class<?>> findClasses() throws IOException {
	if (classPath.isEmpty()) {
	  // returning an empty list
	  return matchList;
	}

	int classPathSize = classPath.size();
	for (int i = 0; i < classPathSize; i++) {
	  ClassPathElement element = getClassPathElement(classPath.get(i));

	  if (element.classOffset?.endsWith(".class")) {
		String className = element.classOffset.substring(0, element.classOffset.length() - 6).replace('/', '.')
		if (className.startsWith(".")) {
		  className = className.substring(1)
		}
		try {
		  Class<?> theClass = Class.forName(className, false, classLoader)

		  if (matcher.isMatch(theClass)) {
			matchList.add(theClass);
			registerHit(element.jarOffset, theClass);
		  }

		} catch (ClassNotFoundException e) {
		  // expected to get this hence trace
		  log.trace("Error searching classpath" + e.getMessage());
		} catch (NoClassDefFoundError e) {
		  // expected to get this hence trace
		  log.trace("Error searching classpath" + e.getMessage());
		}
	  }

	}

	if (matchList.isEmpty()) {
	  log.warn("No Entities found in ClassPath using ClassPathReader [" + classPathReader + "] Classpath Searched[" + classPath + "]");
	}

	return matchList;
  }

  @Override
  Set<String> getJarHits() {
	return this.jarHits
  }

  @Override
  Set<String> getPackageHits() {
	return this.packageHits
  }

  /**
   * Register where matching classes where found.
   * <p>
   * Could use this info to speed up future searches.
   * </p>
   */
  private void registerHit(String jarFileName, Class<?> cls) {
	if (jarFileName != null) {
	  jarHits.add(jarFileName);
	}
	Package pkg = cls.getPackage();
	if (pkg != null) {
	  packageHits.add(pkg.getName());
	} else {
	  packageHits.add("");
	}
  }
  /**
   * Initialize all resources under current ClassPath
   */
  private void initClassPaths() {

	try {

	  Object[] rawClassPaths = readPath(classLoader);

	  if (rawClassPaths == null || rawClassPaths.length == 0) {
		log.warn("BatheClassPath is EMPTY using ClassPathReader [" + classPathReader + "]");
		return;
	  }

	  for (int i = 0; i < rawClassPaths.length; i++) {

		// check for a jarfile with a manifest classpath (e.g. maven surefire)
		List<URI> classPathFromManifest = getClassPathFromManifest(rawClassPaths[i]);

		if (classPathFromManifest.isEmpty()) {
		  classPath.add(rawClassPaths[i]);
		} else {
		  classPath.addAll(classPathFromManifest);
		}
	  }

	  if (rawClassPaths.length == 1) {
		// look to add an 'outer' jar when it contains a manifest classpath
		if (!classPath.contains(rawClassPaths[0])) {
		  classPath.add(rawClassPaths[0]);
		}
	  }

	  if (log.isDebugEnabled()) {
		for (Object entry : classPath) {
		  log.debug("Classpath Entry: {}", entry);
		}
	  }

	} catch (Exception e) {
	  throw new RuntimeException("Error trying to read the classpath entries", e);
	}
  }

  /**
   * If URL and actually a jarfile with manifest return the derived classpath.
   */
  private static List<URI> getClassPathFromManifest(Object classPathElement) {

	try {
	  if (classPathElement instanceof URL) {
		String[] fileNames = (classPathElement as URL).file.split(":")
		String[] jarFileAndOffset = fileNames[1].split("!/")
		JarFile jarFile = new JarFile(new File(jarFileAndOffset[0]))
		JarFile jarEntryFile = jarFile.getNestedJarFile(jarFile.getJarEntryData(jarFileAndOffset[1]))

		List<URI> urlList = new ArrayList<URI>()

		try {
		  Iterator<JarEntryData> iterator = jarEntryFile.iterator()
		  while (iterator.hasNext()) {
			JarEntryData jarEntryData = iterator.next()
			if (jarEntryData.name.toString().endsWith(".class")) {
			  urlList.add(new URI((classPathElement as URL).file + jarEntryData.name))
			}
		  }
		  return urlList
		} finally {
		  jarFile.close()
		  jarEntryFile.close()
		}
	  }
	  return Collections.emptyList();

	} catch (IOException e) {
	  return Collections.emptyList();
	}
  }


  private ClassPathElement getClassPathElement(Object classPathEntry) throws MalformedURLException {

	URL fileUrl = null;

	if (URI.class.isInstance(classPathEntry)) {
	  fileUrl = ((URI) classPathEntry).toURL();

	} else if (!URL.class.isInstance(classPathEntry)) {
	  // assumed to be a file path
	  return new ClassPathElement(classPathEntry.toString());

	} else {
	  fileUrl = (URL) classPathEntry;
	}

	if (!fileUrl.getPath().contains("!")) {
	  return new ClassPathElement(new File(fileUrl.getFile()));
	}

	// jar:file:..../file.war!/WEB-INF/classes typically
	String[] parts = fileUrl.getPath().split("!");
	String fileName = parts[0];
	String jarOffset = parts[1];
	String classOffset = (parts.length > 2 && parts[2].endsWith(".class")) ? parts[2] : null;

	if (fileName.startsWith("file:")) {
	  fileName = fileName.substring("file:".length());
	}

	return new ClassPathElement(new File(fileName), jarOffset, classOffset);
  }

  private static File decodePath(File classPath) {

	try {
	  String charsetName = Charset.defaultCharset().name();

	  // URL Decode the path replacing %20 to space characters.
	  String path = URLDecoder.decode(classPath.getAbsolutePath(), charsetName);
	  return new File(path);

	} catch (UnsupportedEncodingException e) {
	  throw new RuntimeException(e);
	}
  }

  /**
   * Element that has both underlying file and ! jarOffset.
   */
  private static class ClassPathElement {

	final File classPath;
	final String jarOffset;
	final String classOffset;

	ClassPathElement(String path) {
	  this(new File(path));
	}

	ClassPathElement(File file) {
	  this(file, null, null);
	}

	ClassPathElement(File file, String jarOffset, String classOffset) {
	  classPath = decodePath(file);
	  this.jarOffset = jarOffset;
	  this.classOffset = classOffset;
	}

	public String toString() {
	  return classPath.getAbsolutePath();
	}

  }

}
