package nz.co.objectivity.common.ebean

import com.avaje.ebean.config.PropertiesWrapper
import com.avaje.ebean.config.ServerConfig

public class EbeanServerConfig extends ServerConfig {

	public EbeanServerConfig() {
		loadSettings(new SystemPropertyWrapper())
	}

	@Override
	public void loadFromProperties(Properties properties) {
		loadSettings(new SystemPropertyWrapper())
	}

	@Override
	protected void loadDataSourceSettings(PropertiesWrapper p) {
		dataSourceConfig.loadSettings(p)
	}

	@Override
	boolean isDefaultServer() {
		return true
	}

}
