package nz.co.objectivity.common.ebean

import com.bluetrainsoftware.classpathscanner.ResourceScanListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class EbeanResourceScanListener implements ResourceScanListener {

	private static final Logger log = LoggerFactory.getLogger(EbeanResourceScanListener)

	Properties ebeanProperties = new Properties()

	private long startTime

	boolean scanned = false

	List<ResourceScanListener.ScanResource> resources = new ArrayList<ResourceScanListener.ScanResource>()

	@Override
	List<ResourceScanListener.ScanResource> resource(List<ResourceScanListener.ScanResource> scanResources) throws Exception {
		resources.clear();
		for (ResourceScanListener.ScanResource scanResource : scanResources) {
			int position = scanResource.resourceName.lastIndexOf('/')
			String resourceName = scanResource.resourceName.substring(position + 1)
			if ('ebean.properties'.equalsIgnoreCase(resourceName)) {
				log.info("Loading ${scanResource.getResolvedUrl().path}")
				resources.add(scanResource);
			}
		}
		return resources;
	}

	@Override
	void deliver(ResourceScanListener.ScanResource desire, InputStream inputStream) {
		Properties properties = new Properties()
		try {
			properties.load(inputStream)
			if (log.isDebugEnabled()) {
				log.debug("Loaded ${desire.getResolvedUrl().path}")
				properties.each { String key, String value ->
					log.debug("${key} : ${value}")
				}
			}
		} catch (IOException ex) {
			log.error("Failed to load {}", desire.getResolvedUrl().toString())
		}
		ebeanProperties.putAll(properties)
	}

	@Override
	ResourceScanListener.InterestAction isInteresting(ResourceScanListener.InterestingResource interestingResource) {
		return ResourceScanListener.InterestAction.ONCE
	}

	@Override
	void scanAction(ResourceScanListener.ScanAction action) {
		if (action == ResourceScanListener.ScanAction.STARTING) {
			ebeanProperties = new Properties()
			startTime = System.currentTimeMillis()
			scanned = false
		} else if (action == ResourceScanListener.ScanAction.COMPLETE) {
			long endTime = System.currentTimeMillis()
			scanned = true
			log.info('Scaned all ebean.properties to load {} properties, and takes {} seconds', ebeanProperties.size(), (endTime - startTime) / 1000)
		}
	}
}
