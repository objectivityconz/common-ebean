package nz.co.objectivity.common.ebean

import com.avaje.ebean.config.PropertiesWrapper
import com.bluetrainsoftware.classpathscanner.ClasspathScanner
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class SystemPropertyWrapper extends PropertiesWrapper {

	private static final Logger log = LoggerFactory.getLogger(SystemPropertyWrapper)

	private Properties ebeanProperties = new Properties()

	private EbeanResourceScanListener scanListener = new EbeanResourceScanListener()

	public static final Map<String, String> DEFAULT_DATASOURCE = [
			"databaseDriver": "oracle.jdbc.driver.OracleDriver",
			"minConnections": "1",
			"maxConnections": "25",
			"heartbeatsql"  : "select count(*) from dual",
			"isolationlevel": "read_committed"
	]

	public SystemPropertyWrapper() {
		super("dataSource", "default", new Properties())
		ClasspathScanner.getInstance().registerResourceScanner(scanListener)
		ClasspathScanner.getInstance().scan(getClass().getClassLoader(), true)
		loadEbeanProperties()
	}

	@Override
	public SystemPropertyWrapper withPrefix(String prefix) {
		return new SystemPropertyWrapper()
	}

	@Override
	String getServerName() {
		return "default";
	}

	@Override
	String get(String key, String defaultValue) {
		return getKey(key, defaultValue)
	}

	protected String getKey(String key, String defaultValue) {
		String value = getValueWithPrefix("dataSource", key)

		if (!value) {
			value = getValueWithPrefix("ebean", key)
		}

		if (log.isDebugEnabled()) {
			log.debug("Loaded property '{}' with value '{}'", key, "password".equalsIgnoreCase(key)? "**" : value ?: defaultValue)
		}

		return value ?: defaultValue
	}

	/**
	 * System -> classpath:ebean.properties -> DEFAULT -> propeties -> propertyMap
	 */
	protected String getValueWithPrefix(String prefix, String key) {

		String value = getValueWithPrefixFromProperties(prefix, key, System.properties)

		if (!value) {
			loadEbeanProperties()
			value = getValueWithPrefixFromProperties(prefix, key, ebeanProperties)
		}

		if (!value) {
			value = getValueWithPrefixFromProperties(prefix, key, DEFAULT_DATASOURCE)
		}

		if (!value) {
			value = getValueWithPrefixFromProperties(prefix, key, this.properties)
		}

		if (!value) {
			value = getValueWithPrefixFromProperties(prefix, key, this.propertyMap.asProperties())
		}

		return value

	}

	protected String getValueWithPrefixFromProperties(String prefix, String key, Map map) {
		Properties localProperties = new Properties()
		localProperties.putAll(map)
		getValueWithPrefixFromProperties(prefix, key, localProperties)
	}

	protected String getValueWithPrefixFromProperties(String prefix, String key, Properties thisProperties) {

		String value = thisProperties.getProperty(key, null)

		if (!value) {
			value = thisProperties.getProperty("${prefix}.${key}", null)
		}

		if (!value) {
			value = thisProperties.getProperty("${prefix}.${key.toLowerCase()}", null)
		}

		if (!value) {
			value = thisProperties.getProperty("${prefix.toLowerCase()}.${key.toLowerCase()}", null)
		}

		return value
	}

	protected void loadEbeanProperties() {
		if (scanListener.scanned) {
			this.ebeanProperties.putAll(scanListener.ebeanProperties)
		}
	}

}
