# common-ebean

This is a common library to supporting Ebean.

This library provides following features.

* Build in default values 
* Build in ClasspathScanner to load all ebean.properties into System.properties
* Improve the propertyWrapper to load the property value from System.properties firstly
* Provide default EntityRepository as DAO feature.
* Provide BatheClasspathReader to load all Entities in the final WAR file
